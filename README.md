# README #

Student: Jiacheng Wei
Email: jwei@uoregon.edu


----------------------
  
  ```
  ## Author: John Doe, jdoe@uoregon.edu ##
  ```
  
* Copy the credentials-skel.ini file to credentials.ini, then edit it to contain correct information including your Bitbucket repository URL. `credentials.ini` should NOT be under version control (exclude it using your .gitignore file)
* Commit and push ALL your changes to github (except those not under revision control)
* Test deployment in other environments. Deployment should work "out of the box" with this command sequence: 

  ```
  git clone <yourGitRepository> <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ```
  
* Alternatively, use the script under "tests" folder to test the expected outcomes in an automated fashion. It is accompanied by README file and comments (inside tests.sh) explaining how to test your code.
* Check and revise your `credentials/credentials.ini` file. My grading robots will read this. Be precise. My grading robots are not very good at guessing what you meant to write.
* Turn in the `credentials.ini` file in Canvas. My grading robots will use this file to access your github repository.   

### Grading Rubric ###

* Your code works as expected: 100 points

* For every wrong functionality (i.e., (a), (b), and (c) above), 20 points will be docked off. 

* If none of the functionalities work, 40 points will be given. Assuming the credentials.ini is submitted with the correct URL of your repo.

* If credentials.ini is missing, 0 will be assigned.

### Who do I talk to? ###

* Maintained by Ram Durairajan, Steven Walton
* Use our Piazza group for questions. Make them public unless you have a good reason to make them private, so that everyone benefits from answers and discussion. 
